<?php
    require_once '../model/Database.php';
    require_once '../model/ProductTable.php';
    require_once '../model/CustomerTable.php';
    require_once '../model/countriesTable.php';
    require_once '../model/admin_db.php';

    require_once '../util/Util.php';

    class AdminController {
        private $action;

        public function __construct() {
            $this->action = '';
            $this->db = new Database();
            if (!$this->db->isConnected()) {
                $error_message = $this->db->getErrorMessage();
                include '../view/errors/database_error.php';
                exit();
            }
        }

        public function invoke() {
            // get the action to be processed
            $this->action = Util::getAction($this->action);


            if (!isset($_SESSION['is_valid_admin']) && $this->action != 'login') {
                $this->action = 'login';
                $fl=1;
            }

            switch ($this->action) {
                case 'under_construction':
                    include '../view/under_construction.php';
                    break;
                case 'list_products':
                    $this->processListProducts();
                    break;
                case 'delete_product':
                    $this->processDeleteProduct();
                    break;
                case 'show_add_form':
                    $this->processShowAddForm();
                    break;
                case 'add_product':
                    $this->processAddProduct();
                    break;
                case 'customer_search':
                    $this->processCustomerSearch();
                    break;
                case 'display_customer':
                    $this->processDisplayCustomer();
                    break;
                case 'update_customer':
                    $this->processUpdateCustomer();
                    break;
                case 'display_customers':
                    $this->processDisplayCustomers();
                    break;
                case 'login':
                    $admin_table = new adminTable($this->db);
                    $user= filter_input(INPUT_POST, 'userName');
                    $password = filter_input(INPUT_POST, 'password');
                    if ($admin_table->is_valid_admin_login($user, $password)) {
                        $_SESSION['is_valid_admin'] = true;
                        $_SESSION['admin_user'] = $user;
                        $this->processAdminMenu(); 
                    } else {
                        if(isset($fl)){
                            $message = '';
                        }else{
                            $message = 'Invalid username or password.'; 
                        }

                        $user = filter_input(INPUT_POST, 'userName');
                        include '../view/admin/admin_login.php';
                    }
                    break;
                case 'admin_logout':
                    unset($_SESSION['is_valid_admin']);
                    unset($_SESSION['admin_user']);
                    
                    $user = filter_input(INPUT_POST, 'userName');
                    $message = 'You have been logged out.';
                    include '../view/admin/admin_login.php'; 
                    break;
                default:
                    $this->adminLoginStatusCheck();
                    break;
            }
        }

        /****************************************************************
        * Process Request
        ***************************************************************/

        private function adminLoginStatusCheck(){
            if(isset($_SESSION["is_valid_admin"])){
                $this->processAdminMenu(); 
            }else{
                $this->processAdminLogin(); 
            }

        }

        private function processAdminLogin(){
            $user = filter_input(INPUT_POST, 'userName');
            include '../view/admin/admin_login.php';
        }
        private function processAdminMenu() {
            include '../view/admin/admin_menu.php';
        }

        private function processListProducts() {
            $product_table = new ProductTable($this->db);
            $products = $product_table->get_products();
            include '../view/admin/list_products.php';
        }

        private function processDeleteProduct() {
            $product_code = filter_input(INPUT_POST, 'product_code');
            $product_table = new ProductTable($this->db);
            $product_table->delete_product($product_code);
            header("Location: .?action=list_products");
        }

        private function processShowAddForm() {
            include '../view/admin/product_add.php';
        }

        private function processAddProduct() {
            $code = filter_input(INPUT_POST, 'code');
            $name = filter_input(INPUT_POST, 'name');
            $version = filter_input(INPUT_POST, 'version', FILTER_VALIDATE_FLOAT);
            $release_date = filter_input(INPUT_POST, 'release_date');

            // Validate the inputs
            if ( $code === NULL || $name === FALSE ||
                $version === NULL || $version === FALSE ||
                $release_date === NULL) {
                $error = "Invalid product data. Check all fields and try again.";
                include('../view/errors/error.php');
            } else {
                $product_table = new ProductTable($this->db);
                $product_table->add_product($code, $name, $version, $release_date);
                header("Location: .?action=list_products");
            }
        }

        private function processCustomerSearch() {
            $last_name = '';
            $customers = array();
            include '../view/admin/customer_search.php';
        }

        private function processDisplayCustomer() {
            $customer_id = filter_input(INPUT_POST, 'customer_id', FILTER_VALIDATE_INT);
            $customer_table = new CustomerTable($this->db);
            $customer = $customer_table->get_customer($customer_id);
            $countries_table = new countriesTable($this->db);
            $countries=$countries_table->get_countries();

            include '../view/admin/customer_display.php';
        }

        private function processUpdateCustomer() {
            $customer_id = filter_input(INPUT_POST, 'customer_id', FILTER_VALIDATE_INT);
            $first_name = filter_input(INPUT_POST, 'first_name');
            $last_name = filter_input(INPUT_POST, 'last_name');
            $address = filter_input(INPUT_POST, 'address');
            $city = filter_input(INPUT_POST, 'city');
            $state = filter_input(INPUT_POST, 'state');
            $postal_code = filter_input(INPUT_POST, 'postal_code');
            $country_code = filter_input(INPUT_POST, 'country_code');
            $phone = filter_input(INPUT_POST, 'phone');
            $email = filter_input(INPUT_POST, 'email');
            $password = filter_input(INPUT_POST, 'password');

            $error=array();

           if(empty($first_name)){
                $error['fastName']='Required.';
            }
            elseif(!$this->lengthCheck($first_name,1,50)){
                $error['fastName']='Must have at least 1 and less than 51 characters.';
            }

            if(empty($last_name)){
                $error['lastName']='Required.';
            }
            elseif(!$this->lengthCheck($last_name,1,50)){
                $error['lastName']='Must have at least 1 and less than 51 characters.';
            }

            if(empty($address)){
                $error['address']='Required.';
            }
            elseif(!$this->lengthCheck($address,1,50)){
                $error['address']='Must have at least 1 and less than 51 characters.';
            }

            if(empty($city)){
                $error['city']='Required.';
            }
            elseif(!$this->lengthCheck($city,1,50)){
                $error['city']='Must have at least 1 and less than 51 characters.';
            }

            if(empty($state)){
                $error['state']='Required.';
            }
            elseif(!$this->lengthCheck($state,1,50)){
                $error['state']='Must have at least 1 and less than 51 characters.';
            }

            if(empty($password)){
                $error['password']='Required.';
            }
            elseif(!$this->lengthCheck($password,6,20)){
                $error['password']='Must have at least 6 and less than 21 characters.';
            }

            if(empty($postal_code)){
                $error['postalCode']='Required.';
            }
            elseif(!$this->lengthCheck($postal_code,1,20)){
                $error['postalCode']='Must have at least 1 and less than 21 characters.';
            }

            if(empty($phone)){
                $error['phone']='Required.';
            }
            elseif(!$this->phoneNumberCheck($phone)){
                $error['phone']='Use (999) 999-9999 format.';
            }

            if(empty($email)){
                $error['email']='Required.';
            }
            elseif(!$this->checkEmail($email)){
                $error['email']='Invalid email address.';
            }

            if(empty($error)){
                $customer_table = new CustomerTable($this->db);
                $customer_table->update_customer($customer_id, $first_name, $last_name,
                    $address, $city, $state, $postal_code, $country_code,
                    $phone, $email, $password);
                $customer_table = new CustomerTable($this->db);
                $customers = $customer_table->get_customers_by_last_name($last_name);
                include '../view/admin/customer_search.php';
            }else{
                $customer_id = filter_input(INPUT_POST, 'customer_id', FILTER_VALIDATE_INT);
                $customer_table = new CustomerTable($this->db);
                $customer = $customer_table->get_customer($customer_id);
                $countries_table = new countriesTable($this->db);
                $countries=$countries_table->get_countries();
                include '../view/admin/customer_display.php';
            }
        }

        private function processDisplayCustomers() {
            $last_name = filter_input(INPUT_POST, 'last_name');
            if (empty($last_name)) {
                $message = 'Please enter a last name.';
            } else {
                $customer_table = new CustomerTable($this->db);
                $customers = $customer_table->get_customers_by_last_name($last_name);
                if (count($customers) == 0) {
                    $message = 'No customer found';
                }
            }
            include '../view/admin/customer_search.php';
        }

        private function lengthCheck($string,$minimum,$maximum){
            if(strlen($string)<$minimum || strlen($string)>$maximum){
                return False;
            }else{
                return True;
            }
        }

        private function phoneNumberCheck($number){
            if(preg_match("/^\(\d{3}\)\s{0,1}\d{3}-\d{4}$/",$number)){
                $str =  explode(" ",$number);
                if(count($str)==2){
                    return true; 
                }

            }else{
                return false;
            }
        }

        private function checkEmail($email){
            if(preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $email)){
                return true;
            }else{
                return false;
            }
        }
    }

?>