<?php
session_set_cookie_params(0);
session_start();
require_once '../util/Util.php';
require_once 'CustomerController.php';

$controller = new CustomerController();
$controller->invoke();
?>