<?php
    require_once '../model/Database.php';
    require_once '../model/ProductTable.php';
    require_once '../model/CustomerTable.php';
    require_once '../model/RegistrationTable.php';
    require_once '../util/Util.php';

    class CustomerController {
        private $action;

        public function __construct() {
            $this->action = '';
            $this->db = new Database();
            if (!$this->db->isConnected()) {
                $error_message = $this->db->getErrorMessage();
                include '../view/errors/database_error.php';
                exit();
            }
        }

        public function invoke() {
            // get the action to be processed
            $this->action = Util::getAction($this->action);

            switch ($this->action) {
                case 'customer_login':
                    $this->processCustomerLogin();
                    break;
                case 'get_customer':
                    $this->processGetCustomer();
                    break;
                case 'register_product':
                    $this->processRegisterProduct();
                    break;
                case 'customer_logout':
                    $this->processCustomerLogout();
                    break;
                default:
                    $this->processCustomerLogin();
                    break;
            }
        }

        /****************************************************************
        * Process Request
        ***************************************************************/
        private function processCustomerLogin() {
            if(isset($_SESSION["customer_login"]) && $_SESSION["customer_login"]['status']==1 ){
                $product_table = new ProductTable($this->db);
                $products = $product_table->get_products();
                include '../view/customer/product_register.php';
            }else{
                $email = filter_input(INPUT_POST, 'email');
                include '../view/customer/customer_login.php';
            }

        }

        private function processCustomerLogout(){
            unset($_SESSION['customer_login']);   // Clear all session data from memory

            $message = "You have sussessfully logged out.";
            $email = filter_input(INPUT_POST, 'email');
            include '../view/customer/customer_login.php';
        }

        private function processGetCustomer() {
            $email = filter_input(INPUT_POST, 'email');
            $password= filter_input(INPUT_POST, 'password');
            $customer_table = new CustomerTable($this->db);
            $customer = $customer_table->get_customer_by_email($email);
            if ($customer == false) {
                $message = 'Invalid username or password.'; 
                $email = filter_input(INPUT_POST, 'email');
                include '../view/customer/customer_login.php';
            }else{
                if(password_verify($password, $customer['password'])){
                    $_SESSION["customer_login"]['status']=1;
                    $_SESSION["customer_login"]['customerID']=$customer['customerID'];
                    $_SESSION["customer_login"]['firstName']=$customer['firstName'];
                    $_SESSION["customer_login"]['lastName']=$customer['lastName'];
                    $_SESSION["customer_login"]['email']=$customer['email'];


                    $product_table = new ProductTable($this->db);
                    $products = $product_table->get_products();
                    include '../view/customer/product_register.php';
                }
                else{
                    $message = 'Invalid username or password.'; 
                    $email = filter_input(INPUT_POST, 'email');
                    include '../view/customer/customer_login.php';
                }

            }
        }

        private function processRegisterProduct() {
            $customer_id = filter_input(INPUT_POST, 'customer_id', FILTER_VALIDATE_INT);
            $product_code = filter_input(INPUT_POST, 'product_code');
            $registration_table = new RegistrationTable($this->db);
            $registration_table->add_registration($customer_id, $product_code);
            $message = "Product ($product_code) was registered successfully.";
            include '../view/customer/product_register.php';
        }
    }

?>