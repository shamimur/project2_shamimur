<?php 
    include '../util/secure_conn.php'; 
    include '../view/shared/header.php';
?>
<main>

    <h2>Customer Login</h2>
    <p style="color: red"><?php if(isset($message)){echo $message;} ?></p>
    <p>You must login before you can register a product.</p>
    <!-- display a search form -->
    <form action="." method="post" id="aligned">
        <input type="hidden" name="action" value="get_customer">
        <label>Email:</label>
        <input type="text" name="email" value="<?php echo htmlspecialchars($email); ?>">
        <br>
        <label>Password:</label>
        <input type="password" name="password" value="<?php echo @$_POST['password']; ?>">
        <br>
        <label>&nbsp;</label>
        <input type="submit" value="Login">
    </form>

</main>
<?php include '../view/shared/footer.php'; ?>