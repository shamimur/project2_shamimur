<?php
    include '../view/shared/header.php'; 
    include '../util/secure_conn.php'; 
?>

<main>

    <h2>Admin Login</h2>
    <p style="color: red"><?php if(isset($message)){echo $message;} ?></p>
    <!-- display a search form -->
    <form action="." method="post" id="aligned">
        <input type="hidden" name="action" value="login">
        <label>User Name:</label>
        <input type="text" name="userName" value="<?php echo htmlspecialchars($user); ?>">
        <br>
        <label>Password:</label>
        <input type="password" name="password" value="<?php echo @$_POST['password']; ?>">
        <br>
        <label>&nbsp;</label>
        <input type="submit" value="Login">
    </form>

</main>
<?php include '../view/shared/footer.php'; ?>