<?php
    include '../util/secure_conn.php'; 
    include '../view/shared/header.php'; 
?>
<main>

    <!-- display a table of customer information -->

    <h2>View/Update Customer</h2>
    <form action="." method="post" id="aligned">
        <input type="hidden" name="action" value="update_customer">
        <input type="hidden" name="customer_id" 
            value="<?php echo htmlspecialchars($customer['customerID']); ?>">

        <label>First Name:</label>
        <input type="text" name="first_name" 
            value="<?php if(isset($_POST['first_name'])){echo $_POST['first_name'];}else{echo htmlspecialchars($customer['firstName']);} ?>"><span class="error"><?php if(isset($error['fastName'])){echo $error['fastName']; } ?></span><br>

        <label>Last Name:</label>
        <input type="text" name="last_name" 
            value="<?php if(isset($_POST['last_name'])){echo $_POST['last_name'];}else{echo htmlspecialchars($customer['lastName']);} ?>"><span class="error"><?php if(isset($error['lastName'])){echo $error['lastName']; } ?></span><br>

        <label>Address:</label>
        <input type="text" name="address" 
            value="<?php if(isset($_POST['address'])){echo $_POST['address'];}else{echo htmlspecialchars($customer['address']);} ?>" size="50"><span class="error"><?php if(isset($error['address'])){echo $error['address']; } ?></span><br>

        <label>City:</label>
        <input type="text" name="city" 
            value="<?php if(isset($_POST['city'])){echo $_POST['city'];}else{echo htmlspecialchars($customer['city']);} ?>"><span class="error"><?php if(isset($error['city'])){echo $error['city']; } ?></span><br>

        <label>State:</label>
        <input type="text" name="state" 
            value="<?php if(isset($_POST['state'])){echo $_POST['state'];}else{echo htmlspecialchars($customer['state']);} ?>"><span class="error"><?php if(isset($error['state'])){echo $error['state']; } ?></span><br>

        <label>Postal Code:</label>
        <input type="text" name="postal_code" 
            value="<?php if(isset($_POST['postal_code'])){echo $_POST['postal_code'];}else{echo htmlspecialchars($customer['postalCode']);} ?>"><span class="error"><?php if(isset($error['postalCode'])){echo $error['postalCode']; } ?></span><br>

        <label>Country Code:</label>
        <select name="country_code">
            <?php
                foreach($countries as $c){

                ?>
                <option <?php if($c['countryCode']==$customer['countryCode']){?>selected="selected"<?php } ?> value="<?php echo htmlspecialchars($c['countryCode']); ?>"><?php echo htmlspecialchars($c['countryName']); ?></option>
                <?php
                }
            ?>
        </select>
        <!--<input type="text" name="country_code" value="<?php echo htmlspecialchars($customer['countryCode']); ?>">-->
        <br>

        <label>Phone:</label>
        <input type="text" name="phone" value="<?php if(isset($_POST['phone'])){echo $_POST['phone'];}else{echo htmlspecialchars($customer['phone']);} ?>"> <span class="error"><?php if(isset($error['phone'])){echo $error['phone']; } ?></span><br>

        <label>Email:</label>
        <input type="text" name="email" 
            value="<?php if(isset($_POST['email'])){echo $_POST['email'];}else{echo htmlspecialchars($customer['email']);} ?>" size="50"><span class="error"><?php if(isset($error['email'])){echo $error['email']; } ?></span><br>

        <label>Password:</label>
        <input type="text" name="password" 
            value="<?php if(isset($_POST['password'])){echo $_POST['password'];}else{echo '******'; } ?>"><span class="error"><?php if(isset($error['password'])){echo $error['password']; } ?></span><br>

        <label>&nbsp;</label>
        <input type="submit" value="Update Customer"><br>
    </form>
    <p><a href=".?action=customer_search">Search Customers</a></p>

</main>
<?php include '../view/shared/footer.php'; ?>