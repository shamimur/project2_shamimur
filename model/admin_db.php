<?php
    require_once 'Database.php';

    class adminTable{
        private $db;

        public function __construct($db) {
            $this->db = $db;
        }
        function is_valid_admin_login($user, $password) {
            $query = 'SELECT * FROM administrators
            WHERE username = :user';
            $statement = $this->db->getDB()->prepare($query);
            $statement->bindValue(':user', $user);
            $statement->execute();
            $row  = $statement->fetch();
            $statement->closeCursor();
            
            $hash = $row['password'];
            return password_verify($password, $hash);
        }

    }

?>