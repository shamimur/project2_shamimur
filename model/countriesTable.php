<?php
    require_once 'Database.php';

    class countriesTable{
        private $db;

        public function __construct($db) {
            $this->db = $db;
        }

        function get_countries() {
            $query = 'SELECT * FROM countries';
            $statement = $this->db->getDB()->prepare($query);
            $statement->execute();
            $countries = $statement->fetchAll();
            $statement->closeCursor();
            return $countries;
        }

    }
?>
